
package sysexp.builders;

import java.io.IOException;
import sysexp.modele.Regles;

/**
 * Le director
 * 
 * @author siriman
 */
public class Director {
    
    /**
     * 
     */
    protected final Builder builder;
    protected Regles produit;
    
    
    /**
     * 
     * @param builder 
     */
    public Director( Builder builder ){
        this.builder = builder;
    }
    
    /**
     * 
     * @return 
     */
    public Builder getBuilder(){
        return builder;
    }
    
    /**
     * 
     * @throws java.io.IOException
     */
    public void construct() throws IOException {
        // Verification de la base de connaissance : il s'agit d'une operation a risque
	// qui doit etre encagee dans un traite-exception.
        if( builder.verifier()){
            //System.out.println("Tout est Correct");
            produit = builder.lireProduit();
            return;
        }
                
        // L'expression est incorrecte : il faut afficher ce qui pose
        // probleme.
        final String ligne = builder.lexical.lireLigne();
        final int position = builder.lexical.lirePosition();
        final String message = builder.lireErrorMsg()+ " Erreur (entre crochets) en ligne : " + 
            builder.lecteur.getLineNumber()+ 
            "\n" +
            ligne.substring(0, position - 1) + 
            "[" +
            ligne.charAt(position - 1) +
            "]" +
            ligne.substring(position, ligne.length()) +
            "\n";
        System.err.println(message); 
    }
    
    public Regles getResult(){
        return produit;
    }
    
    
}
