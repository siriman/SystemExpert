
package sysexp.modele;

/**
 * Classe representant une premisse affimative.
 * 
 * @author siriman
 */
public class PremisseAffirmation extends Forme {

    /**
     * Constructeur lgique.
     * 
     * @param nom le nom du fait.
     */
    public PremisseAffirmation(String nom) {
        super(nom);
    }
    

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
}
