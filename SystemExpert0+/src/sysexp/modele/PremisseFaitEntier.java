
package sysexp.modele;

/**
 * Classe representant une premisse entiere avec un nom de fait entier comme 
 * partie droite.
 * 
 * @author siriman
 */
public class PremisseFaitEntier extends GeneriqueEntiereSymbolique < String > {

    
    /**
     * Le comparateur.
     */
    protected ComparateurEntier comparateur;
    
    /**
     * Constructeur logique
     * 
     * @param nom le nom du fait.
     * @param comparateur le comparateur.
     * @param partieDroite la partie droite de la premisse.
     */
    public PremisseFaitEntier(String nom, ComparateurEntier comparateur, String partieDroite) {
        super(nom, partieDroite);
        this.comparateur = comparateur;
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }

    /**
     * Accesseur.
     * 
     * @return{@link #comparateur} 
     */
    public ComparateurEntier lireComparateur() {
        return comparateur;
    }
    
}
