
package sysexp.modele;

/**
 * Classe representant un fait entier.
 * 
 * @author siriman
 */
public class FaitsEntiers implements Faits {
    
    /**
     * Le nom du fait.
     */
    protected String nom;
    
    /**
     * La valeur du fait.
     */
    protected long valeur;
    
    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param valeur la valeur du fait.
     */
    public FaitsEntiers( String nom, long valeur ){
        this.nom = nom;
        this.valeur = valeur;
    }
    
    
    @Override
    public String lireNom() {
        return nom;
    }
    
    /**
     * 
     * @return {@link #valeur} 
     */
    public long lireValeur(){
        return valeur;
    }    
}
