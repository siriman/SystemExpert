
package sysexp.modele;

/**
 * Classe representant une premisse symbolique avec une constante symbolique
 * comme partie droite.
 * 
 * @author siriman
 */
public class PremisseConstanteSymbolique extends GeneriqueEntiereSymbolique< String >{

    /**
     * Le comparateur.
     */
    protected ComparateurSymbolique comparateur;
    
    /**
     * Constructeur logique
     * 
     * @param nom le nom du fait.
     * @param comparateur le comparateur.
     * @param partieDroite la partie droite de la premisse.
     */
    public PremisseConstanteSymbolique(String nom, ComparateurSymbolique comparateur, String partieDroite) {
        super(nom, partieDroite);
        this.comparateur = comparateur;
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
    
    /**
     * Accesseur
     * @return {@link #comparateur}
     */
    public ComparateurSymbolique lireComparateur(){
        return comparateur;
    }    
}
