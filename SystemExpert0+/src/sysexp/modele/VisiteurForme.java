
package sysexp.modele;

/**
 * Interface permettant de visiter chaque Forme concrete,
 * Cette classe implemente le behavioral design pattern visitor.
 * 
 * @author siriman, tamim
 */
public interface VisiteurForme {
    
    /**
     * 
     * @param premisse une premisse 
     * @return true 
     * @throws sysexp.modele.DivisionParZeroException 
     * @throws sysexp.modele.IncoherenceException 
     */
    boolean visite( PremisseConstanteEntiere premisse ) throws DivisionParZeroException, IncoherenceException;
    
    /**
     * 
     * @param premisse une PremisseFaitEntier
     */
    boolean visite( PremisseFaitEntier premisse );
    
    /**
     * 
     * @param premisse une PremisseConstanteSymbolique
     */
    boolean visite( PremisseFaitSymbolique premisse );
    
    /**
     * 
     * @param premisse une PremisseConstanteSymbolique
     */
    boolean visite( PremisseConstanteSymbolique premisse );
    
    /**
     * 
     * @param premisse une PremisseAffirmation
     */
    boolean visite( PremisseAffirmation premisse );
    
    /**
     * 
     * @param premisse une PremisseNegation
     */
    boolean visite( PremisseNegation premisse );
    
    /**
     * 
     * @param conclusion une ConclusionEntiere
     */
    boolean visite( ConclusionEntiere conclusion ) throws DivisionParZeroException, IncoherenceException;
    
    /**
     * 
     * @param conclusion une ConclusionFaitEntier
     */
    boolean visite( ConclusionFaitEntier conclusion );
    
    
    /**
     * 
     * @param conclusion une ConclusionConstanteSymbolique
     * @return  
     */
    boolean visite( ConclusionConstanteSymbolique conclusion );
    
    /**
     * 
     * @param conclusion une ConclusionSymboliqueFait
     */
    boolean visite( ConclusionSymboliqueFait conclusion );
    
    /**
     * 
     * @param conclusion une ConclusionAffirmative
     */
    boolean visite( ConclusionAffirmative conclusion );
    
    /**
     * 
     * @param conclusion une ConclusionNegative
     */
    boolean visite( ConclusionNegative conclusion );
}
