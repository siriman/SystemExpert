
package sysexp.modele;

/**
 * Classe representant un fait boolean.
 * 
 * @author siriman
 */
public class FaitsBooleens implements Faits {
    
    /**
     * Le nom du fait.
     */
    protected String nom;
    
    /**
     * La valeur du fait.
     */
    protected Boolean valeur;
    
    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param valeur la valeur du fait.
     */
    public FaitsBooleens( String nom, boolean valeur ){
        this.nom = nom;
        this.valeur = valeur;
    }
    
    @Override
    public String lireNom() {
        return nom;
    }
    
    /**
     * Accesseur
     * 
     * @return {@link #valeur}
     */
    public Boolean lireValeur(){
        return valeur;
    }
    
}
