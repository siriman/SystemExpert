
package sysexp.builders.lorraine;

import java.io.IOException;
import java.io.LineNumberReader;


/**
 * Classe represenant l'analyseur lexical
 * 
 * @author TRAORE Siriman 21308390  
 */
public class Lexical {
    
    /**
     * Constructeur logique
     * 
     * @param lecteur la valeur de {@link Lexical#lecteur}
     */
    public Lexical( LineNumberReader lecteur ){
        this.lecteur = lecteur;
        position = 0;
        ligne = "";
    }
    
    
    /**
     * Accesseur de la ligne
     * 
     * @return le numero de la ligne
     */
    public String lireLigne(){
        return ligne;
    }
    
    /**
     * Accesseur
     * 
     * @return le numero de la colonne
     */
    public int lirePosition(){
        return position;
    }

    
    /**
     * Extrait puis retourne le jeton suivant.
     *
     * @return le jeton suivant.
     * @throws java.io.IOException  sur le flot d'entree ne peut etre lu.
     */
    public Jeton suivant() throws IOException {

	// Si nous sommes a la fin du flot, il faut retourner le jeton associe
	// a la fin d'expression.
	if (! avancer()) {
	    return FabriqueJeton.finExpression();
	}

	// Caractere correspondant a la position courante.
	char caractere = ligne.charAt(position); 

	// Il faut identifier le jeton.
	switch(caractere) {   
 
	case '(': // Parenthese ouvrante.
	    position ++;
	    return FabriqueJeton.parentheseOuvrante();

	case ')': // Parenthese fermante.
	    position ++;
	    return FabriqueJeton.parentheseFermante();

	case '+': // Operateur d'addition.
	    position ++;
	    return FabriqueJeton.operateurPlus();

	case '-': // Operateur de soustraction.
	    position ++;
	    return FabriqueJeton.operateurMoins();

	case '*': // Operateur de multiplication.
	    position ++;
	    return FabriqueJeton.operateurMultiplie();

	case '/': // Operateur de division ou comparateurDifferent.

            if( position+1 < ligne.length()&&( ligne.charAt(position+1) == '=' )){
                position = position+2;
                return FabriqueJeton.comparateurDifferent( ligne.substring(position - 2, 
							 position) );
            }
            
            position ++;
	    return FabriqueJeton.operateurDivise();
            
        case '>': // Comparateur superieur ou comparateurSuperieurOuEgal.

            if( position+1 < ligne.length()&&( ligne.charAt(position+1) == '=' )){
                position = position+2;
                return FabriqueJeton.comparateurSupEgal( ligne.substring(position - 2, 
							 position ) );
            }
            position++;
            return FabriqueJeton.comparateurSuperieur();
            
        case '<': // Comparateur inferieur ou comparateurInfreieurOuEgal.
            
            if( position+1 < ligne.length()&&( ligne.charAt(position+1) == '=' ) ){
                position = position+2;
                return FabriqueJeton.comparateurInfEgal( ligne.substring(position - 2, 
							 position) );
            }            
            
            position++;
            return FabriqueJeton.comparateurInferieur();
            
        case '=': // Comparateur superieur ou egale
            position++;
            return FabriqueJeton.comparateurEgalite();
            
        case '|': // Caractere Pipe
            position++;
            return FabriqueJeton.caracterePipe();
               
        case 'Ɛ': // Epsilon
            position++;
            return FabriqueJeton.epsilon();
            
        case ',': // Virgule
            position++;
            return FabriqueJeton.virgule();
         
        case ';': // Point Virgule
            position++;
            return FabriqueJeton.pointVirgule();
            
        case '_': // Caractere underscore
            position++;
            return FabriqueJeton.underscore();
        
        case 'f':
            int fin = position+1;
            if( estMotFaits_(fin) ){
                fin = position+6;
                
                if( estMotBooleens( fin )){
                    position = fin+8;
                    return FabriqueJeton.faits_Booleens();
                }
                
                if( estMotSymboliques( fin )){
                    position = fin+11;
                    return FabriqueJeton.faits_Symboliques();
                }

                if( estMotEntiers( fin )){
                    position = fin+7;
                    return FabriqueJeton.faits_Entiers();
                } 

                return extraireLettre(); 
            }
            return extraireLettre();            
                 
        
	default: // Chiffre ou lettre ou bien representation inconnue.
	    if (Character.isDigit(caractere)) {
                
                if( caractere == '0'){ // Chiffre 0
                    position++;
                    return FabriqueJeton.zero();
                }
                return extraireChiffre();
	    }
            
            if (Character.isLetter(caractere)) {
                switch(caractere){

                case 'e':

                    if( (position+1 < ligne.length())&&( ligne.charAt(position+1) == 't' )){
                        position = position+2;
                        return FabriqueJeton.operateurEt(); //ligne.substring(position-2, position));
                    }
                    
                    return extraireLettre(); 
                    
                case 's':

                    if( position+1 < ligne.length()&&( ligne.charAt(position+1) == 'i' )){
                        position = position+2;
                        return FabriqueJeton.operateurSi();
                    }  
                
                    return extraireLettre(); 

                case 'n':

                    if( position+1 < ligne.length()&&( ligne.charAt(position+1) == 'o' )){
                        if( position+2 < ligne.length()&&( ligne.charAt(position+2) == 'n' )){
                            position = position+3;
                            return FabriqueJeton.operateurNegation();                
                        }
                    }
                    
                    return extraireLettre(); 

                case 'a':
                    if( position+1 < ligne.length()&&( ligne.charAt(position+1) == 'l' )){
                        if( position+2 < ligne.length()&&( ligne.charAt(position+2) == 'o' )){
                            if( position+3 < ligne.length()&&( ligne.charAt(position+3) == 'r' )){
                                if( position+4 < ligne.length()&&( ligne.charAt(position+4) == 's' )){
                                    position = position+5;
                                    return FabriqueJeton.operateurAlors();                         
                                }

                            }
                        }
                    }
                    return extraireLettre();  
                        
                default:
                    return extraireLettre();

                }
	    }
            
	    // C'est la representation inconnue.
	    position ++;
	    return FabriqueJeton.inconnu(ligne.substring(position - 1, 
							 position));
	}

    }

    
    
    /**
     * Tente d'avancer dans le flot.
     *
     * @return true si nous avons pu avancer dans le flot sinon false.
     * @throws java.io.IOException  sur le flot d'entree ne peut etre lu.
     */
    protected boolean avancer() throws IOException {

	// Tant que nous sommes sur une ligne vide, il faut en lire d'autres
	// jusqu'à obtenir une ligne non vide ou bien atteindre la fin du flot.
	while (true) {

	    // Tant que nous ne sommes pas à la fin de la ligne et que le 
	    // caractere courant est un separateur, nous avancons dans la ligne.
	    while (position < ligne.length() && 
		   Character.isWhitespace(ligne.charAt(position))) {
		position ++;
	    }

	    // Nous sommes a la fin de la ligne : il faut en lire une autre.
	    if (position == ligne.length()) {

		// Lecture d'une nouvelle ligne.
		ligne = lecteur.readLine();

		// La ligne est vide : nous sommes a la fin du flot.
		if (ligne == null) {
		    return false;
		}

		// Repositionnement au debut de la nouvelle ligne.
		position = 0;

	    }

	    // Nous sommes sur le premier caractere d'un jeton : il est temps
	    // de sortie de la boucle.
	    else {
		return true;
	    }

	}
    }

    
    
    /**
     * Extrait un entier du flot d'entree.
     * 
     * @return un jeton entier
     */
    protected Jeton extraireChiffre() {

	// Nous nous placons juste derriere la position courante.
	int fin = position + 1;

	// Nous avons dans la ligne jusqu'a en attendre la fin ou bien tomber
	// sur un caractere qui n'est plus un chiffre.
	while (fin < ligne.length() && Character.isDigit(ligne.charAt(fin))) {
	    fin ++;
	}

	// Memorisation de la position du premier chiffre dans la ligne.
	int debut = position;

	// Positionnement sur le premier caractere situe derriere le dernier
	// chiffre.
	position = fin;

	// Extraction.
	return FabriqueJeton.chiffre_Positif(ligne.substring(debut, fin));

    }
    
    
    /**
     * Extrait une lettre du flot d'entree.
     * 
     * @return un jeton lettre
     */
    protected Jeton extraireLettre() {

	// Nous nous placons juste derriere la position courante.
	int fin = position + 1;

	// Nous avons dans la ligne jusqu'a en attendre la fin ou bien tomber
	// sur un caractere qui n'est plus une lettre.
	while (fin < ligne.length() && Character.isLetter(ligne.charAt(fin))) {
	    fin ++;
	}

	// Memorisation de la position de la premiere lettre dans la ligne.
	int debut = position;

	// Positionnement sur le premier caractere situe derriere la derniere
	// lettre.
	position = fin;

	// Extraction.
	return FabriqueJeton.lettre(ligne.substring(debut, fin));
    }
    
    protected boolean estMotFaits_( int fin ){
        if( (fin < ligne.length())&&( ligne.charAt(fin) == 'a') ){
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 'i') ){
                return false;
            } 
            fin++;
            if(!(fin < ligne.length()&&( ligne.charAt(fin) == 't' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 's' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == '_' )) ){
                return false;
            }
            fin++;
            return true;
        }
        return false;   
    }
    
    protected boolean estMotBooleens( int fin ){
        if(  (fin < ligne.length())&&( ligne.charAt(fin) == 'b') ){
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 'o') ){
                return false;
            } 
            fin++;
            if(!(fin < ligne.length())&&( ligne.charAt(fin) == 'o' ) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'l' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'e' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'e' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'n' )) ){
                return false;
            }  
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 's' )) ){
                return false;
            }
            fin++;
            return true;
        }
        return false;
        
    }

    protected boolean estMotSymboliques( int fin ){
        if( (fin < ligne.length())&&( ligne.charAt(fin) == 's') ){
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'y')) ){
                return false;
            } 
            fin++;
            if(!(fin < ligne.length()&&( ligne.charAt(fin) == 'm' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'b' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'o' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'l' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'i' )) ){
                return false;
            }  
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'q' )) ){
                return false;
            }  
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'u' )) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 'e' )) ){
                return false;
            }  
            fin++;
            if(! (fin < ligne.length()&&( ligne.charAt(fin) == 's' )) ){
                return false;
            }
            fin++;
            return true;
        }
        return false;
    }
    
    protected boolean estMotEntiers( int fin ){
        if( (fin < ligne.length())&&( ligne.charAt(fin) == 'e') ){
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 'n') ){
                return false;
            } 
            fin++;
            if(!(fin < ligne.length())&&( ligne.charAt(fin) == 't' ) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 'i' ) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 'e' ) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 'r' ) ){
                return false;
            }
            fin++;
            if(! (fin < ligne.length())&&( ligne.charAt(fin) == 's' ) ){
                return false;
            }
            fin++;
            return true;
        }
        return false;        
    } 
    
    /**
     * 
     */
    LineNumberReader lecteur;
    
    /**
     * 
     */
    int position;
    
    /**
     * 
     */
    String ligne;

    
}