
package sysexp.modele;

/**
 * Classe representant une expression entiere, elle implemente le pattern 
 * interpreter.
 * 
 * @author siriman
 */
public abstract class ExpressionEntiere {
    
    /**
     * Methode equivalent de la methode interpret du couplage Interpreter+Composite.
     * 
     * @param contexte la base de fait.
     * @return la valeur de l'expression.
     * @throws DivisionParZeroException est lance quand {@link #operandeDroite} est inferieur a zero.
     * @throws IncoherenceException
     */
    public abstract long interpret( BaseDefaits contexte  ) throws DivisionParZeroException, IncoherenceException;

}
