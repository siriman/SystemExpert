
package sysexp.modele;

/**
 * Classe representant la classe mere des constantes,
 * elle representante les terminaux dans le couplage Interpreter+Composite.
 * 
 * @param < E > type générique de {@link #valeur}
 * @author siriman
 */
public abstract class AbstractFeuille< E > extends ExpressionEntiere {
    
    /**
     * La valeur de la feuille.
     */
    protected E valeur;
    
    /**
     * Constructeur logique.
     * 
     * @param valeur la {@link #valeur} 
     */
    public AbstractFeuille( E valeur ){
        this.valeur = valeur;
    }
    
    /**
     * Methode representant la methode interpret du couplage Interpreter+Composite,
     * elle utilse {@link evaluer( BaseDefait contexte) } d'ou l'implementation 
     * du design pattern Template Methode.
     * 
     * @param contexte la base de fait.
     * @return la valeur de la constante.
     * @throws IncoherenceException
     */
    @Override
    public long interpret( BaseDefaits contexte ) throws IncoherenceException {
        return evaluer(contexte);
    }
    
    /**
     * Methode evaluant une expression,
     * elle aide l'implementation du pattern Template Methode.
     * 
     * @param contexte la base de fait.
     * @return la valeur de la constante.
     * @throws IncoherenceException
     */
    public abstract long evaluer( BaseDefaits contexte ) throws IncoherenceException ;
}
