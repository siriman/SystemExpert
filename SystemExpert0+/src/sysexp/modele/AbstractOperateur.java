
package sysexp.modele;

/**
 * Classe representant la classe mere des operateurs,
 * elle representante les non-terminaux dans le couplage Interpreter+Composite.
 * 
 * @author siriman
 */
public abstract class AbstractOperateur extends ExpressionEntiere {
   
    /**
     * Constructeur logique.
     * 
     * @param operandeGauche l'expression de droite de l'operateur.
     * @param operandeDroite l'expression de gauche de l'operateur.
     */
    public AbstractOperateur( ExpressionEntiere operandeGauche, ExpressionEntiere operandeDroite ){
        this.operandeGauche = operandeGauche;
        this.operandeDroite = operandeDroite;
    }
    
    /**
     * Methode representant la methode interpret du couplage Interpreter+Composite,
     * elle utilse {@link evaluer( BaseDefait contexte) } d'ou l'implementation 
     * du design pattern Template Methode.
     * 
     * @param contexte la base de fait.
     * @return la valeur de la constante.
     * @throws DivisionParZeroException division par zero.
     * @throws IncoherenceException erreur d'incoherence.
     */
    @Override
    public long interpret( BaseDefaits contexte ) throws DivisionParZeroException, IncoherenceException {
        return evaluer(contexte);
    }
    
    /**
     * Methode qui evalue deux expressions.
     * 
     * @param contexte la base de fait.
     * @return le resultat de l'evaluation de {@link operandeGauche} par {@link operandeDroite}.
     * @throws DivisionParZeroException  
     * @throws IncoherenceException  
     */
    public abstract long evaluer( BaseDefaits contexte ) throws DivisionParZeroException, IncoherenceException ;
    
    /**
     * L'operande de gauche.
     */
    protected ExpressionEntiere operandeGauche;
    
    /**
     * L'operande de droite.
     */
    protected ExpressionEntiere operandeDroite;

}
