
package sysexp.modele;

/**
 * Classe representant le type generique des premisses symboliques et entiere.
 * 
 * @author siriman
 * @param <E> le generique
 */
public abstract class GeneriqueEntiereSymbolique< E > extends Forme {
    
    /**
     * La partie droite de la premisse.
     */
    protected E partieDroite;
    
    /**
     * Classe generique representant les premisses et les conclusions entieres 
     * et symboliques
     * 
     * @param faits
     * @param partieDroite 
     */
    public GeneriqueEntiereSymbolique(String faits, E partieDroite ) {
        super(faits);
        this.partieDroite = partieDroite;
    }
    
    /**
     * Accesseur
     * 
     * @return la {@link #partieDroite}
     */
    public E lirePartieDroite(){
        return partieDroite;
    }
    
}
