
package sysexp.modele;

/**
 * Classe representant une conclusion symbolique avec un nom de fait comme 
 * partie droite.
 * 
 * @author siriman, tamim
 */
public class ConclusionSymboliqueFait extends GeneriqueEntiereSymbolique< String > {
    
    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param partieDroite la partie droite
     */
    public ConclusionSymboliqueFait(String nom, String partieDroite) {
        super(nom, partieDroite);
    }
    
    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
    
}
