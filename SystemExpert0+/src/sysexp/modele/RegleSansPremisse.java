
package sysexp.modele;

/**
 * Classe representant une regle sans premisse.
 * 
 * @author siriman
 */
public class RegleSansPremisse extends Regles {
    
    
    /**
     * Constructeur logique.
     * 
     * @param conclusion la conclusion 
     */
    public RegleSansPremisse( Forme conclusion ){
        super(conclusion);
    }

    @Override
    public boolean estDeclenchable(VisiteurForme visiteur) {
        return ! declenchee;
    }
    
    
    
}
