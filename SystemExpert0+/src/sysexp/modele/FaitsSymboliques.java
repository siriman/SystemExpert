
package sysexp.modele;

/**
 * Classe representant un fait symbolique.
 * 
 * @author siriman
 */
public class FaitsSymboliques implements Faits {

    /**
     * Le nom du fait.
     */
    protected String nom;
    
    /**
     * La valeur du fait.
     */
    protected String valeur;
    
    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param valeur la valeur du fait.
     */
    public FaitsSymboliques( String nom, String valeur ){
        this.nom = nom;
        this.valeur = valeur;
    }   
    
    @Override
    public String lireNom() {
        return nom;
    }
    
    /**
     * Accesseur.
     * 
     * @return {@link #valeur}
     */
    public String lireValeur(){
        return valeur;
    }
    
}
