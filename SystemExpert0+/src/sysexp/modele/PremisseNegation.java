
package sysexp.modele;

/**
 * Classe representant une premisse negative.
 * 
 * @author siriman
 */
public class PremisseNegation extends Forme {

    /**
     * Constructeur logique
     * 
     * @param faits le nom du fait.
     */
    public PremisseNegation(String faits) {
        super(faits);
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }   
}
