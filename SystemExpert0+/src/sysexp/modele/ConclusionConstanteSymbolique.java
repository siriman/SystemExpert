
package sysexp.modele;

/**
 * Classe representant une conclusion symbolique.
 * 
 * @author siriman, tamim
 */
public class ConclusionConstanteSymbolique extends GeneriqueEntiereSymbolique< String > {

    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param partieDroite la partie droite
     */
    public ConclusionConstanteSymbolique(String nom, String partieDroite) {
        super(nom, partieDroite);
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
    
}
