
package sysexp.modele;

/**
 * Classe representant l'operateur d'addition.
 * 
 * @author siriman
 */
public class OperateurAdd extends AbstractOperateur {

    /**
     * Constructeur logique.
     * 
     * @param operandeGauche expression de gauche.
     * @param operandeDroite expression de droite.
     */
    public OperateurAdd(ExpressionEntiere operandeGauche, ExpressionEntiere operandeDroite) {
        super(operandeGauche, operandeDroite);
    }

    /**
     * Methode faisant l'addition de deux expressions.
     * 
     * @param contexte la base de fait.
     * @return le resultat de l'addition de {@link #operandeGauche } et de {@link #operandeDroite }.
     * @throws DivisionParZeroException erreur de division par zero. 
     * @throws IncoherenceException erreur semantique. 
     */
    @Override
    public long evaluer( BaseDefaits contexte ) throws DivisionParZeroException, IncoherenceException {
        return operandeGauche.interpret(contexte) + operandeDroite.interpret(contexte);
    }

    
}
