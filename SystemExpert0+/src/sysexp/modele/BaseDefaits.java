
package sysexp.modele;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Classe representant la base de fait.
 * 
 * @author siriman, tamim
 */
public class BaseDefaits {
    
    /**
     * Le dico representant la base de fait.
     */
    protected HashMap <String, Faits> dico;
    
    /**
     * Constructeur logique.
     */
    public BaseDefaits(){
        dico = new HashMap <String, Faits>();
    }
    
    /**
     * Methode permettant de rechercher un fait dans la base de faits via son nom.
     * 
     * @param nom le nom associe au fait.
     * @return le fait recherche.
     */
    public Faits rechercher( String nom ){
        return dico.get(nom);
    }
    
    /**
     * Methode de d'ajouter un fait dans la base de faits.
     * 
     * @param nom le nom du fait.
     * @param fait le fait a ajouter.
     */
    public void ajouter( String nom, Faits fait ){
        dico.put(nom, fait);
    }
    
    /**
     * Indique si la base de fait contient le fait passer en parametre.
     *  
     * @param nom le nom du fait.
     * @return true si la base de fait contient le fait, false sinon. 
     */
    public boolean existeFait( String nom ){
        return dico.containsKey(nom);
    }
    
    /**
     * Methode donnant les faits contenus dans la base de faits
     * 
     * @return la valeur des faits de la base de faits.
     */
    public ArrayList<String> balayer(){
        
        
        ArrayList listeFait = new ArrayList<>();
        
        
        for( String cle : dico.keySet() ){
            //listeFait.add(dico.get(cle));
            Faits fait = dico.get(cle);
            listeFait.add(fait.lireNom());
        }
        return listeFait;
    }        
        
}

    
    
