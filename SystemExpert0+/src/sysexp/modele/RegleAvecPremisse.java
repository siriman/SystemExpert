
package sysexp.modele;

import java.util.LinkedList;

/**
 * Classe representant une regle avec premisse. 
 * 
 * @author siriman, tamim
 */
public class RegleAvecPremisse extends Regles {
    
    /**
     * La condition de la regle.
     */
    protected LinkedList<Forme> condition;
    
    /**
     * Constructeur logique.
     * 
     * @param condition la condition de la regle.
     * @param conclusion la conclusion de la regle.
     */
    public RegleAvecPremisse( LinkedList< Forme > condition, Forme conclusion ){
        super( conclusion );
        this.condition = condition;
    }

    
    /**
     * Accesseur
     * 
     * @return {@link #condition}
     */
    public LinkedList<Forme> lireCondition(){
        return condition;
    }


    @Override
    public boolean estDeclenchable(VisiteurForme visiteur) throws DivisionParZeroException, IncoherenceException {
	// Si la regle a deja ete declenchee, elle n'est plus declenchable.
	if (declenchee) {
	    return false;
	}

	// Pour etre declenchable, tous les premisses d'une regle doivent se 
	// trouver dans la base de faits.
	for (Forme premisse : condition) {
            if( !  premisse.accept(visiteur) ){
                return false;
            }
	}

	// La base de faits contenait bien tous les premisses de cette regle.
	return true;
    }
    
}
