
package sysexp.client;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import sysexp.builders.Builder;
import sysexp.builders.Director;
import sysexp.modele.BaseDefaits;
import sysexp.modele.DivisionParZeroException;
import sysexp.modele.IncoherenceException;
import sysexp.modele.Regles;


/**
 * Classe representant le programme principal.
 */
public class Client {

    /**
     * Point d'entree de la JVM.
     *
     * @param args - les arguments de la ligne de commandes.
     */
    @SuppressWarnings("empty-statement")
    public static void main(String[] args) throws IOException {

	// La ligne de commandes est vide : l'utilisateur demande de l'aide.
	if (args.length == 0) {
	    System.out.println("Usage: java DemoLexical nomdefichier");
	    return;
	}

	// Plus d'un argument sur la ligne de commandes : l'utilisateur fait
	// n'importe quoi.
	if (args.length != 1) {
	    System.err.println("Nombre d'arguments incorrect.");
	    return;
	}

	// Tentative de connexion d'un flot d'entree au fichier.
	LineNumberReader lecteur = null;
	try {
	    lecteur = new LineNumberReader(new FileReader(args[0]));
	}
	catch(FileNotFoundException e) {
	    System.err.println("Le fichier [" + args[0] + "] n'existe pas.");
	    return;
	}
        
        Builder aConcreteBuilder = new Builder(lecteur);
        Director aDirector = new Director(aConcreteBuilder);
        Regles baseDeRegles = null;
        try{
            aDirector.construct();
            baseDeRegles = aDirector.getResult();
        }
        catch(IOException e) {
	    System.err.println("Impossible de lire [ "+ args[0] + "]");
	    return;
	}
        BaseDefaits baseDefaits = new BaseDefaits();
        try {
            while(baseDeRegles.iterer(baseDefaits));
        }
        catch ( final IncoherenceException e ) {
            System.err.println("Erreur d'Incoherence");
            return ;
        }
        catch ( final DivisionParZeroException e ) {
            System.err.println("Erreur d'Incoherence");
            return ;
        }
                
        System.out.println("La base de faits est:\n"+baseDefaits.balayer());
        
    }
}