
package sysexp.modele;

/**
 * Classe representant une erreur d'incoherence
 * 
 * @author siriman
 */
public class IncoherenceException extends Exception {

    /**
     * 
     * Constructeur par defaut.
     */
    public IncoherenceException() {
    
    }

    /**
     * Constructeur logique.
     *
     * @param msg le detail de l'erreur.
     */
    public IncoherenceException(String msg) {
        super(msg);
    }
}
