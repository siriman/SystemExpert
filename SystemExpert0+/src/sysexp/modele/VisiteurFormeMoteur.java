
package sysexp.modele;

/**
 * Classe representant le visiteur concret.
 * 
 * @author siriman, tamim
 */
public class VisiteurFormeMoteur implements VisiteurForme {
    
    
    /**
     * La base de faits.
     */
    protected BaseDefaits baseDefaits;
    
    /**
     * Vraie si la derniere premisse visitee est verifiee dans la base de fait,
     * sinon faux.
     */
    protected boolean estVerifiee;
    
    /**
     * Vraie si la derniere conclusion visitee a pu s'executer dans la base de 
     * fait, sinon faux.
     */
    protected boolean estExecutee;

    /**
     * Le code d'erreur.
     */
    protected Erreur codeErreur;
    
    /**
     * Constructeur logique.
     * 
     * @param baseDefaits la base de fait 
     */
    public VisiteurFormeMoteur( BaseDefaits baseDefaits ){
        this.baseDefaits = baseDefaits;
    }
    
    @Override
    public boolean visite( PremisseConstanteEntiere premisse ) throws 
            DivisionParZeroException, IncoherenceException {
        String nomFait = premisse.lireNomFait();
        if( (! baseDefaits.existeFait(nomFait)) ){
            return false;
        }
        
        long gauche = ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur();
        long droite = premisse.lirePartieDroite().interpret(baseDefaits); 
        
        switch( premisse.lireComparateur() ) {
            case Egal:
                return gauche == droite;
            case Superieur:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() >
                  premisse.lirePartieDroite().interpret(baseDefaits);    
            case Inferieur:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() <
                  premisse.lirePartieDroite().interpret(baseDefaits);    
            case InferieurOuEgal:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() <=
                  premisse.lirePartieDroite().interpret(baseDefaits);    
                
            case SuperieurOuEgal:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() >=
                  premisse.lirePartieDroite().interpret(baseDefaits);    
            case Different:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() !=
                  premisse.lirePartieDroite().interpret(baseDefaits);    
        }

        return false;
    }
    

    @Override
    public boolean visite( PremisseFaitEntier premisse ) {
        if( (! baseDefaits.existeFait(premisse.lireNomFait()) ) || (! baseDefaits.existeFait(premisse.lirePartieDroite()) ) ){
            return false;
        }
        
        switch( premisse.lireComparateur() ) {
            case Egal:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() ==
                  ( (FaitsEntiers)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur();    
            case Superieur:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() >
                  ( (FaitsEntiers)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur();                
            case Inferieur:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() <
                  ( (FaitsEntiers)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur();
            case InferieurOuEgal:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() <=
                  ( (FaitsEntiers)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur();
                
            case SuperieurOuEgal:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() >=
                  ( (FaitsEntiers)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur();
            case Different:
                return ( (FaitsEntiers)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur() !=
                  ( (FaitsEntiers)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur();                
        }
        
        return false;
    }
    
    @Override
    public boolean visite( PremisseFaitSymbolique premisse ) {
        if( (! baseDefaits.existeFait(premisse.lireNomFait()) ) || (! baseDefaits.existeFait(premisse.lirePartieDroite()) ) ){
            return false;
        }
        
        switch( premisse.lireComparateur() ) {
            case Egalite:
                return ( (FaitsSymboliques)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur().equals(
                  ( (FaitsSymboliques)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur());
            case Different:
                return !( ( (FaitsSymboliques)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur().equals(
                  ( (FaitsSymboliques)baseDefaits.rechercher(premisse.lirePartieDroite())).lireValeur()) );
        }
        
        return false;
    }

    @Override
    public boolean visite( PremisseConstanteSymbolique premisse ) {
        
        if(! baseDefaits.existeFait(premisse.lireNomFait()) ){
            return false;
        }
        switch( premisse.lireComparateur() ) {
            case Egalite:
                return ( (FaitsSymboliques)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur().equals(
                    premisse.lirePartieDroite());
            case Different:
                return !( ( (FaitsSymboliques)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur().equals(
                    premisse.lirePartieDroite()) );
        }
        
        return false;
    }

    @Override
    public boolean visite( PremisseAffirmation premisse ) {
        
        if(! baseDefaits.existeFait(premisse.lireNomFait()) ){
            return false;
        }
        return ( (FaitsBooleens)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur();

   }

    @Override
    public boolean visite( PremisseNegation premisse ) {
        if(! baseDefaits.existeFait(premisse.lireNomFait()) ){
            return false;
        }
        return ! ( (FaitsBooleens)baseDefaits.rechercher(premisse.lireNomFait())).lireValeur();
    }

    @Override
    public boolean visite( ConclusionEntiere conclusion ) throws DivisionParZeroException, IncoherenceException {
        String nomFait = conclusion.lireNomFait();
        
        if( baseDefaits.existeFait(nomFait) ) {
            if( ( (FaitsEntiers)baseDefaits.rechercher(nomFait)).lireValeur() ==
                    conclusion.lirePartieDroite().interpret(baseDefaits) ){
                return true;
            } else{
                return false;
            }
        }
        
        baseDefaits.ajouter(nomFait, new FaitsEntiers(nomFait,
                conclusion.lirePartieDroite().interpret(baseDefaits)));
        return true;
    }
    
    @Override
    public boolean visite(ConclusionFaitEntier conclusion) {
        if(! baseDefaits.existeFait(conclusion.lirePartieDroite()) ){
            return false;//La partie droite n'est ecore dasn la base de fait
        }
        
        if( baseDefaits.existeFait(conclusion.lireNomFait()) ) {
            if( ( (FaitsEntiers)baseDefaits.rechercher(conclusion.lireNomFait())).lireValeur() ==
                  ( (FaitsEntiers)baseDefaits.rechercher(conclusion.lirePartieDroite())).lireValeur() ){
                return true;
            } else{
                return false;
            }
        }
        
        baseDefaits.ajouter(conclusion.lireNomFait(), new FaitsEntiers(conclusion.lireNomFait(),
                ((FaitsEntiers)baseDefaits.rechercher(conclusion.lirePartieDroite())).lireValeur()));
        return true;
    }
    
    
    @Override
    public boolean visite( ConclusionConstanteSymbolique conclusion ) {

        if( baseDefaits.existeFait(conclusion.lireNomFait()) ) {
            if( ( (FaitsSymboliques)baseDefaits.rechercher(conclusion.lireNomFait())).lireValeur().equals(
                  conclusion.lirePartieDroite()) ){
                return true;
            } else{
                return false;
            }
        }
        
        baseDefaits.ajouter(conclusion.lireNomFait(), new FaitsSymboliques(conclusion.lireNomFait(),
                conclusion.lirePartieDroite()));
        return true;
    }

    @Override
    public boolean visite( ConclusionSymboliqueFait conclusion ) {
        
        if(! baseDefaits.existeFait(conclusion.lirePartieDroite()) ){
            return false;//La partie droite n'est ecore dasn la base de fait
        }
        
        if( baseDefaits.existeFait(conclusion.lireNomFait()) ) {
            if( ( (FaitsSymboliques)baseDefaits.rechercher(conclusion.lireNomFait())).lireValeur().equals(
                  ( (FaitsSymboliques)baseDefaits.rechercher(conclusion.lirePartieDroite())).lireValeur()) ){
                return true;
            } else{
                return false;
            }
        }
        
        baseDefaits.ajouter(conclusion.lireNomFait(), new FaitsSymboliques(conclusion.lireNomFait(),
                ((FaitsSymboliques)baseDefaits.rechercher(conclusion.lirePartieDroite())).lireValeur()));
        return true;
    }

    @Override
    public boolean visite( ConclusionAffirmative conclusion ) {
        if( baseDefaits.existeFait(conclusion.lireNomFait()) ) {
            if( ( (FaitsBooleens)baseDefaits.rechercher(conclusion.lireNomFait())).lireValeur() ){
                return true;
            } else{
                return false;
            }
        }
        baseDefaits.ajouter(conclusion.lireNomFait(), new FaitsBooleens(conclusion.lireNomFait(), true));
        return true;
        
    }

    @Override
    public boolean visite( ConclusionNegative conclusion ) {
        
        if(! baseDefaits.existeFait(conclusion.lireNomFait()) ){
            if(! ( (FaitsBooleens)baseDefaits.rechercher(conclusion.lireNomFait())).lireValeur() ){
                return true;
            } else{
                return false;
            }
        }        
        baseDefaits.ajouter(conclusion.lireNomFait(), new FaitsBooleens(conclusion.lireNomFait(), false));
        return true;
    }
    
    /**
     * Code d'erreur
     */
    enum Erreur {
        NotError,
        ErrorDivisionParZero,
        ErrorIncoherence
    }
    
}
