
package sysexp.modele;

/**
 * Classe representant une feuille Fait Entier.
 *  
 * @author siriman, tamim
 */
public class FeuilleFaitEntier extends AbstractFeuille< String > {
    
    /**
     * Le constructeur logique.
     * @param nomFait 
     */
    public FeuilleFaitEntier( String nomFait ){
        super(nomFait);
    }

    @Override
    public long evaluer( BaseDefaits contexte ) throws IncoherenceException {

        if( contexte.existeFait(valeur) ) {
            FaitsEntiers fait = (FaitsEntiers)contexte.rechercher(valeur);
            return fait.lireValeur();
        }
        
        throw new IncoherenceException();
    }
    
}
