
package sysexp.modele;

/**
 * Classe representant l'operateur de division.
 * 
 * @author siriman
 */
public class OperateurDiv extends AbstractOperateur {

    /**
     * Constructeur logique.
     * 
     * @param operandeGauche expression de gauche.
     * @param operandeDroite expression de droite.
     */
    public OperateurDiv(ExpressionEntiere operandeGauche, ExpressionEntiere operandeDroite) {
        super(operandeGauche, operandeDroite);
    }

    /**
     * Methode faisant la division de deux expressions.
     * 
     * @param contexte la base de fait.
     * @return le resultat de la division de {@link #operandeGauche} par {@link #operandeDroite}.  
     * @throws DivisionParZeroException est lancee quand {@link #operandeDroite} est egale a zero. 
     * @throws IncoherenceException  
     */
    @Override
    public long evaluer(BaseDefaits contexte) throws DivisionParZeroException, IncoherenceException {
        
        long numerateur = operandeGauche.interpret(contexte);
        long denominateur = operandeDroite.interpret(contexte);
        
        /* Division par zero */
        if( denominateur == 0 ){
            throw new DivisionParZeroException() ;
        }

        return numerateur/denominateur;
        
    }
    
}
