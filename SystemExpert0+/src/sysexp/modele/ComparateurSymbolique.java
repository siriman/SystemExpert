
package sysexp.modele;

/**
 * Comparateur des premisses Symboliques
 * 
 * @author siriman
 */
public enum ComparateurSymbolique {
    Egalite,
    Different
}
