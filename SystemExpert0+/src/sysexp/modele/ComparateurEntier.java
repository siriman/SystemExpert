
package sysexp.modele;

/**
 * Comparateur des premisses entieres.
 * 
 * @author siriman
 */
public enum ComparateurEntier {
    Egal,
    Different,
    Superieur,
    Inferieur,
    SuperieurOuEgal,
    InferieurOuEgal
}
