
package sysexp.modele;

/**
 * Cette classe implemente le behavioral design pattern visitor.
 * 
 * @author siriman
 */
public abstract class Forme {
    
    /**
     * Le nom du fait.
     */
    protected String nom;
    
    /**
     * Constructeur logique
     * 
     * @param faits 
     */
    public Forme( String faits ){
        this.nom = faits;
    }
    
    /**
     * Accesseur
     * 
     * @return {@link #nom} 
     */
    public String lireNomFait(){
        return nom;
    }
    
    /**
     * Equivalant de la methode accept du pattern visitor.
     * 
     * @param visiteur le visiteur.
     * @throws sysexp.modele.DivisionParZeroException 
     * @throws sysexp.modele.IncoherenceException 
     * @return true 
     */
    public abstract boolean accept( VisiteurForme visiteur ) throws DivisionParZeroException, IncoherenceException;
    
}
