
package sysexp.modele;

/**
 * Classe representant une conclusion avec un nom de fait entier. 
 * 
 * @author siriman, tamim
 */
public class ConclusionFaitEntier extends GeneriqueEntiereSymbolique< String > {

    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param partieDroite la partie droite
     */
    public ConclusionFaitEntier( String nom, String partieDroite ) {
        super(nom, partieDroite);
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
    
}
