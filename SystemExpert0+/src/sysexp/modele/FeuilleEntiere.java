
package sysexp.modele;

/**
 * Classe representant une constance entiere.
 * 
 * @author siriman, tamim
 */
public class FeuilleEntiere extends AbstractFeuille< Long > {

    /**
     * Constructeur logique.
     * 
     * @param valeur la valeur.
     */
    public FeuilleEntiere( long valeur ){
        super(valeur);
    }
    

    @Override
    public long evaluer( BaseDefaits contexte ) {
        return valeur;
    }
    
}
