
package sysexp.modele;

/**
 * Classe representant une premisse symbolique avec un nom de fait symbolique
 * comme partie droite.
 * 
 * @author siriman
 */
public class PremisseFaitSymbolique extends GeneriqueEntiereSymbolique < String > {

    
    /**
     * Le comparateur.
     */
    protected ComparateurSymbolique comparateur;
    
    /**
     * Constructeur logique
     * 
     * @param nom le nom du fait.
     * @param comparateur le comparateur.
     * @param partieDroite la partie droite de la premisse.
     */
    public PremisseFaitSymbolique(String nom, ComparateurSymbolique comparateur, String partieDroite) {
        super(nom, partieDroite);
        this.comparateur = comparateur;
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
    
    /**
     * Accesseur
     * 
     * @return {@link #comparateur} 
     */
    public ComparateurSymbolique lireComparateur(){
        return comparateur;
    }
    
}
