
package sysexp.modele;


/**
 * La classe mere representant une regle
 * 
 * @author siriman
 */
public abstract class Regles {
    
    /**
     * Le numero de la regle.
     */
    protected static long numero = 1;
    
    
    /**
     * La regle successeur.
     */
    protected Regles successeur;
    
    /**
     * La conclusion de la regle.
     */
    protected Forme conclusion;
    
    /**
     * Indique si une regle est declenchee ou pas.
     */
    protected boolean declenchee;
    
    
    /**
     * Constructeur logique.
     * 
     * @param conclusion une conclusion.
     */
    public Regles( Forme conclusion  ){
        this.conclusion = conclusion;
        numero++;
    }
        
    /**
     * Accesseur
     * 
     * @return {@link #conclusion }
     */
    public Forme lireConclusion(){
        return conclusion;
    }
    
    /**
     * Accesseur.
     * 
     * @return {@link #numero}
     */
    public long lireNumRegle(){
        return numero;
    }
    
    /**
     * Accesseur.
     * 
     * @return la valeur de {@link #successeur}.
     */
    public Regles lireSuccesseur( ){
        return successeur;
    }
    
    /**
     * Accesseur.
     *
     * @param valeur
     */
    public void ecrireSuccesseur(Regles valeur) {
	successeur = valeur;
    }
    
    /**
     * Accesseur.
     *
     * @return la valeur de {@link #declenchee}.
     */
    public boolean lireDeclenchee() {
	return declenchee;
    }

    /**
     * Accesseur.
     *
     * @param valeur la nouvelle valeur de {@link #declenchee}.
     */
    public void ecrireDeclenchee(boolean valeur) {
	declenchee = valeur;
    }
    
    /**
     * Indique si cette regle possede un successeur dans la chaine de 
     * responsabilites.
     *
     * @return true si cette regle possede un successeur dans la chaine de 
     *   responsabilites sinon false.
     */
    public boolean possedeSuccesseur() {
	return successeur != null;
    }
    
    
    /**
     * Indique si une nouvelle regle a pu etre declenchee, elle implemente le 
     * pattern Template en utilisant {@link estDeclenchable}.
     * 
     * @param baseDefait la base de fait.
     * @return true si une nouvelle regle a pu etre declenchee et false sinon
     */
    public boolean iterer( BaseDefaits baseDefait ) throws DivisionParZeroException, IncoherenceException {
        
        VisiteurForme visiteur = new VisiteurFormeMoteur( baseDefait );
	// Flag indiquant un nouveau declenchement.
	boolean declenchement = false;
        
        // Si cette regle est declenchable alors la declencher en ajoutant sa
        // conclusion a la base de faits.
        if (estDeclenchable(visiteur)) {
            if( conclusion.accept(visiteur) ){
                declenchee = true;
                declenchement = true;
            }
        }

        // Si cette regle possede un successeur dans la chaine alors lui passer
        // la main.
        if (possedeSuccesseur()) {
            return declenchement || successeur.iterer(baseDefait);
        }


	// Sinon, le traitement s'arrete ici.
	return declenchement;
    }
    
    
    /**
     * Indique si une regle est declenchable ou pas.
     * 
     * @param visiteur le visiteur
     * @throws DivisionParZeroException division par zero.
     * @throws IncoherenceException erreur d'incoherence.
     * @return true si la regle est declenchable, false sinon.
     */
    public abstract boolean estDeclenchable( VisiteurForme visiteur ) throws DivisionParZeroException, IncoherenceException;
        
}
