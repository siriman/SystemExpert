
package sysexp.modele;

import java.util.ArrayList;

/**
 * La classe representant la base de regle
 * 
 * @author siriman
 */
public class BaseDeRegles {
    
    /**
     * la base de regle
     */
    protected ArrayList<Regles> baseDeRegles;
    
    
    /**
     * Constructeur logique.
     */
    public BaseDeRegles(){
        baseDeRegles = new ArrayList<Regles>();
    }
    
    /**
     * Methode qui ajouter une regle a la base de regle.
     * @param regle la regle a ajouter
     */
    public void setRegle( Regles regle ){
        baseDeRegles.add(regle);
    }
    
    /**
     * Accesseur
     * 
     * @return {@link baseDeRegles }. 
     */
    public ArrayList<Regles> getBaseDeRegles(){
        return baseDeRegles;
    }
    
}
