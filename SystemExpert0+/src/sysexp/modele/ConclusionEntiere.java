
package sysexp.modele;

/**
 * Classe representant une conclusion entiere.
 * 
 * @author siriman, tamim
 */
public class ConclusionEntiere extends GeneriqueEntiereSymbolique< ExpressionEntiere > {

    
    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     * @param partieDroite la partie droite
     */
    public ConclusionEntiere(String nom, ExpressionEntiere partieDroite) {
        super(nom, partieDroite);
    }

    @Override
    public boolean accept(VisiteurForme visiteur) throws DivisionParZeroException, IncoherenceException {
        return visiteur.visite(this);
    }
    
}
