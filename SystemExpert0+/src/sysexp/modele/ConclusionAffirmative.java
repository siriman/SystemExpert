
package sysexp.modele;

/**
 * Classe representant une conclusion affirmative.
 * 
 * @author siriman, tamim
 */
public class ConclusionAffirmative extends Forme {

    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait.
     */
    public ConclusionAffirmative(String nom) {
        super(nom);
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
}
