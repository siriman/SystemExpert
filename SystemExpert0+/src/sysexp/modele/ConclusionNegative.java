
package sysexp.modele;

/**
 * Classe representant une conclusion negative.
 * 
 * @author siriman, tamim.
 */
public class ConclusionNegative extends Forme {

    /**
     * Constructeur logique.
     * 
     * @param nom le nom du fait
     */
    public ConclusionNegative(String nom) {
        super(nom);
    }

    @Override
    public boolean accept(VisiteurForme visiteur) {
        return visiteur.visite(this);
    }
    
}
