
package sysexp.modele;

/**
 * Classe representant l'operateur soustraction.
 *
 * @author siriman
 */
public class OperateurSub extends AbstractOperateur {

    /**
     * Constructeur logique.
     * 
     * @param operandeGauche expression de gauche.
     * @param operandeDroite expression de droite.
     */
    public OperateurSub(ExpressionEntiere operandeGauche, ExpressionEntiere operandeDroite) {
        super(operandeGauche, operandeDroite);
    }

    /**
     * Methode faisant la soustraction de deux expressions.
     * 
     * @param contexte la base de fait.
     * @return le resultat de la soustraction de {@link operandeGauche} et de {@link operandeDroite}.  
     * @throws DivisionParZeroException est lance quand {@link #operandeDroite} est inferieur a zero.
     * @throws IncoherenceException  
     */
    @Override
    public long evaluer(BaseDefaits contexte) throws DivisionParZeroException, IncoherenceException {
        return operandeGauche.interpret(contexte) - operandeDroite.interpret(contexte);
    }
    
}
