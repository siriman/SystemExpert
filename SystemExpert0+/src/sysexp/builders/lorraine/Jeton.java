
package sysexp.builders.lorraine;

/**
 *Classe representant un Jeton de la grammaire
 * 
 * @author siriman
 */
public class Jeton {
        
    /**
     * Type enumere fortement type representant les differentes types de
     * jetons.
     */
    public enum Type{
        
        OperateurPlus,
	OperateurMoins,
	OperateurMultiplie,
	OperateurDivise,
        OperateurAnd,
        OperateurEt,
        OperateurNegation,
        ComparateurSuperieur,
        ComparateurInferieur,
        ComparateurDifferent,
        ComparateurSuperieurOuEgal,
        ComparateurInferieurOuEgal,
        ComparateurEgalite,
        ParentheseOuvrante,
	ParentheseFermante,
        CaracterePipe,
        Epsilon,
        Virgule,
        PointVirgule,
        Underscore,
        Si,
        Alors,
        Chiffre_Positif,
        Lettre,
        Zero,
        FinExpression,
        MotCleFaits_Booleens,
        MotCleFaits_Symboliques,
        MotCleFaits_Entiers,
        Inconnu
    }
    
    
    /**
     * Constructeur logique
     * 
     * @param type Le type du Jeton
     * @param representation La représentation du Jeton
     */
    protected Jeton( Type type, String representation ){
        this.type = type;
        this.representation = representation;
    }
    
    /**
     * Accesseur
     * 
     * @return Le type du jeton
     */
    public Type lireType(){
        return type;
    }
    
    /**
     * Accesseur
     * 
     * @return La représentation du Jeton 
     */
    public String lireRepresentation(){
        return representation;
    }
    
    /**
     * Indique si ce jeton est associe a superieur.
     *
     * @return true si ce jeton est associe a superieur sinon 
     *   false.
     */
    public boolean estComparateurSuperieur(){
        return type == Type.ComparateurSuperieur;
    }
    
    /**
     * Indique si ce jeton est associe a inferieur.
     *
     * @return true si ce jeton est associe a inferieur sinon 
     *   false.
     */
    public boolean estComparateurInferieur(){
        return type == Type.ComparateurInferieur;
    }
    
    /**
     * Indique si ce jeton est associe au Comparateur Different .
     *
     * @return true si ce jeton est associe au Comparateur Different sinon 
     *   false.
     */
    public boolean estComparateurDifferent(){
        return type == Type.ComparateurDifferent;
    }
    
    /**
     * Indique si ce jeton est associe au ComparateurSuperieurOuEgal .
     *
     * @return true si ce jeton est associe au ComparateurSuperieurOuEgal  sinon 
     *   false.
     */
    public boolean estComparateurSuperieurOuEgal(){
        return type == Type.ComparateurSuperieurOuEgal;
    }
    
    /**
     * Indique si ce jeton est associe au ComparateurInferieurOuEgal .
     *
     * @return true si ce jeton est associe au ComparateurInferieurOuEgal  sinon 
     *   false.
     */
    public boolean estComparateurInferieurOuEgal(){
        return type == Type.ComparateurInferieurOuEgal;
    }
    
    /**
     * Indique si ce jeton est associe au ComparateurEgalite  .
     *
     * @return true si ce jeton est associe au ComparateurEgalite  sinon 
     *   false.
     */
    public boolean estComparateurEgalite(){
        return type == Type.ComparateurEgalite;
    }

    /**
     * Indique si ce jeton est associe a la parenthese ouvrante.
     *
     * @return true si ce jeton est associe a la parenthese ouvrante sinon 
     *   false.
     */
    public boolean estParentheseOuvrante() {
	return type == Jeton.Type.ParentheseOuvrante;
    }

    /**
     * Indique si ce jeton est associe a la parenthese fermante.
     *
     * @return true si ce jeton est associe a la parenthese fermante sinon 
     *   false.
     */
    public boolean estParentheseFermante() {
	return type == Jeton.Type.ParentheseFermante;
    }
    
    
    /**
     * Indique si ce jeton est associe a epsilon.
     *
     * @return true si ce jeton est associe a epsilon sinon 
     *   false.
     */
    public boolean estEpsilon(){
        return type == Type.Epsilon;
    }
    
    /**
     * Indique si ce jeton est associe a virgule.
     *
     * @return true si ce jeton est associe a virgule sinon 
     *   false.
     */
    public boolean estVirgule(){
        return type == Type.Virgule;
    }
    
    /**
     * Indique si ce jeton est associe a PointVirgule.
     *
     * @return true si ce jeton est associe a PointVirgule sinon 
     *   false.
     */
    public boolean estPointVirgule(){
        return type == Type.PointVirgule;
    }
    
    /**
     * Indique si ce jeton est associe a Underscore.
     *
     * @return true si ce jeton est associe a Underscore sinon 
     *   false.
     */
    public boolean estUnderscore(){
        return type == Type.Underscore;
    }
    
    /**
     * Indique si ce jeton est associe a OperateurAnd.
     *
     * @return true si ce jeton est associe a OperateurAnd sinon 
     *   false.
     */
    public boolean estOperateurAnd(){
        return type == Type.OperateurAnd;
    }

    /**
     * Indique si ce jeton est associe a OperateurEt.
     *
     * @return true si ce jeton est associe a OperateurEt sinon 
     *   false.
     */
    public boolean estOperateurEt(){
        return type == Type.OperateurEt;
    }
    
    /**
     * Indique si ce jeton est associe a OperateurPlus.
     *
     * @return true si ce jeton est associe a OperateurPlus sinon 
     *   false.
     */
    public boolean estOperateurPlus() {
        return type == Type.OperateurPlus;
    }

    /**
     * Indique si ce jeton est associe a OperateurMoins.
     *
     * @return true si ce jeton est associe a OperateurMoins sinon 
     *   false.
     */
    public boolean estOperateurMoins() {
        return type == Type.OperateurMoins;
    }

    /**
     * Indique si ce jeton est associe a Multiplie.
     *
     * @return true si ce jeton est associe a Multiplie sinon 
     *   false.
     */
    public boolean estOperateurMultiplie() {
        return type == Type.OperateurMultiplie;
    }

    /**
     * Indique si ce jeton est associe a OperateurDivise.
     *
     * @return true si ce jeton est associe a OperateurDivise sinon 
     *   false.
     */
    public boolean estOperateurDivise() {
        return type == Type.OperateurDivise;
    }
    
    /**
     * Indique si ce jeton est associe a OperateurNegation.
     *
     * @return true si ce jeton est associe a OperateurNegation sinon 
     *   false.
     */
    public boolean estOperateurNegation(){
        return type == Type.OperateurNegation;
    }
    
    /**
     * Indique si ce jeton est associe a OperateurSi.
     *
     * @return true si ce jeton est associe a OperateurSi sinon 
     *   false.
     */
    public boolean estOperateurSi(){
        return type == Type.Si;
    }
    
    /**
     * Indique si ce jeton est associe a Alors.
     *
     * @return true si ce jeton est associe a Alors sinon 
     *   false.
     */
    public boolean estOperateurAlors(){
        return type == Type.Alors;
    }
    
    /**
     * Indique si ce jeton est associe a Chiffre_Positif.
     *
     * @return true si ce jeton est associe a Chiffre_Positif sinon 
     *   false.
     */
    public boolean estChiffre_Positif(){
        return type == Type.Chiffre_Positif;
    }
    
    /**
     * Indique si ce jeton est associe a Zero.
     *
     * @return true si ce jeton est associe a Zero  sinon 
     *   false.
     */
    public boolean estZero(){
        return type == Type.Zero;
    }
    
    /**
     * Indique si ce jeton est associe a Lettre.
     *
     * @return true si ce jeton est associe a Lettre sinon 
     *   false.
     */
    public boolean estLettre(){
        return type == Type.Lettre;
    }
    
    /**
     * Indique si ce jeton est associe a MotCleFaits_Booleens.
     *
     * @return true si ce jeton est associe a MotCleFaits_Booleens sinon 
     *   false.
     */
    public boolean estMotCleFaitsBooleens(){
        return type == Type.MotCleFaits_Booleens;
    }
    
    /**
     * Indique si ce jeton est associe a MotCleFaits_Symboliques.
     *
     * @return true si ce jeton est associe a MotCleFaits_Symboliques sinon 
     *   false.
     */
    public boolean estMotCleFaitsSymboliques(){
        return type == Type.MotCleFaits_Symboliques;
    }
    
    /**
     * Indique si ce jeton est associe a MotCleFaits_Entiers.
     *
     * @return true si ce jeton est associe a MotCleFaits_Entiers sinon 
     *   false.
     */
    public boolean estMotCleFaitsEntiers(){
        return type == Type.MotCleFaits_Entiers;
    }
    
    /**
     * Indique si ce jeton est associe a FinExpression.
     *
     * @return true si ce jeton est associe a FinExpression  sinon 
     *   false.
     */
    public boolean estFinExpression(){
        return type == Type.Inconnu;
    }
    
    /**
     * Indique si ce jeton est associe a Inconnu.
     *
     * @return true si ce jeton est associe a Inconnu  sinon 
     *   false.
     */
    public boolean estInconnu(){
        return type == Type.Inconnu;
    }
    
    
    /**
     * Type du jeton.
     */
    Type type;
    
    /**
     * Représentation du jeton.
     */
    String representation;    
}
