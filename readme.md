###  System Expert 0+ 

#### Auteur
TRAORE Siriman

#### Description

Projet de réalisation d'un Système Expert de Type 0+. J'ai réalisé ce projet dans le cadre de mon L3 Informatique. En plus de l'intelligence artificielle, il ressort les grands concepts du Génie logiciel : la réutilisabilité, les designs patterns

#### Design pattern implémenté dans le projet
- Builder
- Composite
- Interpreter
- Chain Of Responsability
- Visitor
- Template Method

#### Usage :

Lancer l'application avec:
		
		ant -DbaseDeConnaissance=fichier.txt 
