
package sysexp.builders.lorraine;

import java.io.IOException;
import java.util.LinkedList;
import sysexp.builders.lorraine.Jeton;
import sysexp.builders.lorraine.Lexical;
import sysexp.modele.ComparateurEntier;
import sysexp.modele.ComparateurSymbolique;
import sysexp.modele.ConclusionAffirmative;
import sysexp.modele.ConclusionConstanteSymbolique;
import sysexp.modele.ConclusionEntiere;
import sysexp.modele.ConclusionFaitEntier;
import sysexp.modele.ConclusionNegative;
import sysexp.modele.ConclusionSymboliqueFait;
import sysexp.modele.ExpressionEntiere;
import sysexp.modele.FeuilleEntiere;
import sysexp.modele.FeuilleFaitEntier;
import sysexp.modele.Forme;
import sysexp.modele.OperateurAdd;
import sysexp.modele.OperateurDiv;
import sysexp.modele.OperateurMult;
import sysexp.modele.OperateurSub;
import sysexp.modele.PremisseAffirmation;
import sysexp.modele.PremisseConstanteEntiere;
import sysexp.modele.PremisseConstanteSymbolique;
import sysexp.modele.PremisseFaitEntier;
import sysexp.modele.PremisseNegation;
import sysexp.modele.RegleAvecPremisse;
import sysexp.modele.RegleSansPremisse;
import sysexp.modele.Regles;

/**
 * 
 * 
 * @author siriman
 */
public class Syntaxique {
    
    /**
     * Constructeur logique.
     *
     * @param lexical le lecteur la valeur de {@link Syntaxique#lexical}
     */
    public Syntaxique(Lexical lexical) {
	this.lexical = lexical;
        this.faits_Booleens = new LinkedList<String>();
        this.faits_Symboliques = new LinkedList<String>();
        this.faits_Entiers = new LinkedList<String>();
        this.condition = new LinkedList<Forme>();
    }

    /**
     * Accesseur.
     *
     * @return la valeur de {@link Syntaxique#lexical}.
     */
    public Lexical lireLexical() {
	return lexical;
    }
    
    public String lireErrorMsg(){
        return errorMessage;
    }
    
    public Regles lireProduit(){
        return produit;
    }

    
    
    /**
     * Analyse l'expression connectee a l'analyseur lexical.
     *
     * @return true si l'expression est syntaxiquement correcte sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression.
     */
    public boolean verifier() throws IOException {

	// Pre-chargement du premier jeton.
	precharge = lexical.suivant();
	// Appel de la methode associee a la regle "Expression".
	if ( estDeclarations()) {
            if( estRegles() ){
                return true;
            }
            return false;
	}
        
	// Il faut verifier que nous avons atteint la fin du texte.
	return precharge.estFinExpression();
    }

    
    
    /* Structures des règles regissants les declarations de faits */
  
    
    
    /**
     * 
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estDeclarations() throws IOException {
        
        if(! estDeclarationBooleen() ){
            return false;
        }
        
        if(! estDeclarationSymbolique() ){
            return false;
        }
        
        if(! estDeclarationEntiere() ){
            return false;
        }
        
        return true;
    
    }
    
    
    /**
     * 
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estDeclarationBooleen() throws IOException {
    
    
        if(! precharge.estMotCleFaitsBooleens()){
            errorMessage = "Mot cle [faits_Booleens] inexistants";
            return false;
        }
        
        precharge = lexical.suivant();
                
        if(! precharge.estComparateurEgalite()){
            errorMessage = "caractere [=] est absent";
            return false;
        }
        
        precharge = lexical.suivant();

        if(! estFaitsBooleens()){
            return false;
        }
        
        if(! precharge.estPointVirgule()){
            errorMessage = "caractere [;] est absent";
            return false;
        }
        
        precharge = lexical.suivant();
        return true;
        
    }    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estDeclarationSymbolique() throws IOException {
    
        if(! precharge.estMotCleFaitsSymboliques()){
            errorMessage = "Mot cle [faits_symboliques] inexistants";
            return false;
        }
        
        precharge = lexical.suivant();
                
        if(! precharge.estComparateurEgalite()){
            errorMessage = "caractere [=] est absent";
            return false;
        }
        
        precharge = lexical.suivant();

        if(! estFaitsSymboliques()){
            return false;
        }
        
        if(! precharge.estPointVirgule()){
            errorMessage = "caractere [;] est absent";
            return false;
        }
        
        precharge = lexical.suivant();
        return true;
        
    }

    
    /**
     * 
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estDeclarationEntiere() throws IOException {
    
        if(! precharge.estMotCleFaitsEntiers()){
            errorMessage = "Mot cle [faits_entiers] inexistants";
            return false;
        }
        
        precharge = lexical.suivant();
                
        if(! precharge.estComparateurEgalite()){
            errorMessage = "caractere [=] est absent";
            return false;
        }
        
        precharge = lexical.suivant();

        if(! estFaitsEntiers()){
            return false;
        }
        
        if(! precharge.estPointVirgule()){
            errorMessage = "caractere [;] est absent";
            return false;
        }
        
        precharge = lexical.suivant();
        return true;
        
    }

    
    /**
     * Methode associee a la regle "Faits_Booleens".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression. 
     */
    protected boolean estFaitsBooleens() throws IOException {
        
        // La liste de Faits_Booleens est soit vide(epsilon), soit constituée
        // d'une séquence d'une virgule suivi Fait_Booleen
        if( precharge.estEpsilon() ){
            precharge = lexical.suivant();
            return true;
        }
        
        //appel a la regle faits_Booleen = identificateur
        if( estFaitBooleen() ){
        
            this.faits_Booleens.add(identificateur);
            // Sequences eventuelles composees d'une virgule suivi
            // d'un fait symbolique.
            while( precharge.estVirgule() ){

                //on passe le point virgule
                precharge = lexical.suivant();

                // Appel de la methode associee a la regle "faits_Booleens".
                if(! estFaitBooleen()){
                    return false;
                }
                this.faits_Booleens.add(identificateur);
            }
            
            return true;
        }
        
        return false;
        
    }    

    
    /**
     * Methode associee a la regle "Faits_Symboliques".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression. 
     */
    protected boolean estFaitsSymboliques() throws IOException {
        
        // La liste de Faits_Symboliques est soit vide(epsilon), soit constituée
        // d'une séquence d'une virgule suivi fait_symbolique
        if( precharge.estEpsilon() ){
            precharge = lexical.suivant();
            return true;
        }
        
        //appel a la methode fait symbolique = identificateur
        if( estFaitSymbolique() ){
        
            this.faits_Symboliques.add(identificateur);
            // Sequences eventuelles composees d'une virgule suivi
            // d'un fait symbolique.
            while( precharge.estVirgule() ){

                //on passe le point virgule
                precharge = lexical.suivant();

                // Appel de la methode associee a la regle "Fait_Symbolique".
                if(! estFaitSymbolique()){
                    return false;
                }
                this.faits_Symboliques.add(identificateur);
            }
            
            return true;
        }
        
        return false;
        
    }
    
    
    /**
     * Methode associee a la regle "Faits_Entiers".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression. 
     */
    protected boolean estFaitsEntiers() throws IOException {
        
        // La liste de Faits_Entiers est soit vide(epsilon), soit constituée
        // d'une séquence de virgule suivi d'un Fait_Entier
        if( precharge.estEpsilon() ){
            precharge = lexical.suivant();
            return true;
        }
        
        //appel a la methode fait_Entier = identificateur
        if( estFaitEntier() ){
        
            this.faits_Entiers.add(identificateur);
            // Sequences eventuelles composees d'une virgule suivi
            // d'un fait symbolique.
            while( precharge.estVirgule() ){

                //on passe le point virgule
                precharge = lexical.suivant();

                // Appel de la methode associee a la regle "faits_Entiers".
                if(! estFaitEntier()){
                    return false;
                }
                this.faits_Entiers.add(identificateur);
            }
            
            return true;
        }
        
        return false;
        
    }
    
    
    /**
     * Methode associee a la regle "Fait_Booleen".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression. 
     */
    protected boolean estFaitBooleen() throws IOException {
        return estIdentificateur();
    }    
    
    /**
     * Methode associee a la regle "Fait_Symbolique".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression. 
     */
    protected boolean estFaitSymbolique() throws IOException {
        return estIdentificateur();
    } 
    
    /**
     * Methode associee a la regle "Fait_Entier".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression. 
     */
    protected boolean estFaitEntier() throws IOException {
        return estIdentificateur();
    }

    /**
     * 
     * Methode associee a la regle "Identifcateur".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression.
     */
    protected boolean estIdentificateur() throws IOException {
        
        //Appel à methode associe au jeton lettre
        if( ! precharge.estLettre() ){
            errorMessage = "Un identificateur commence par une lettre";
            return false;
        }
        identificateur = precharge.lireRepresentation();
        precharge = lexical.suivant();
        
        // Sequences eventuelles composees d'underscore et d'alphanumerique
	while ( precharge.estUnderscore() || precharge.estLettre() || 
                precharge.estZero() || precharge.estChiffre_Positif() ) {

            if( precharge.estUnderscore()){
                // Passer le caractere underscore.
                identificateur += precharge.lireRepresentation();
                precharge = lexical.suivant();
                // Appel de la methode associee a la regle "Alphanumerique".
                if(! (precharge.estLettre() || precharge.estZero() ||
                        precharge.estChiffre_Positif() ) ) {
                    errorMessage = "[_] doit etre suivi par un alphanumerique";
                    return false;
                }
            } else{
                identificateur += precharge.lireRepresentation();
                precharge = lexical.suivant();                
            }

        }

	// La regle est satisfaite.
	return true;
        
        
    }
    
    
    /**
     * 
     * Methode associee a la regle "Alphanumerique".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression.
     */
    protected boolean estAlphanumerique() throws IOException {
        return precharge.estLettre() || estChiffre();   
    }

    
    protected boolean estChiffre() throws IOException {
        return precharge.estChiffre_Positif() || precharge.estZero();
    } 
    
    /**
     * Methode associee a la regle "Chiffre_Positif".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression.
     */
//    protected boolean estChiffrePositif() throws IOException {
//        return precharge.estChiffre_Positif();//&&(! precharge.estZero());
//    }
    
    
    
    /* La structure d’une énumération de règles dans la grammaire Lorraine */
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estRegles() throws IOException{
        
        if(! estRegle() ){
            return false;
        }
        
        while( estRegle() ){        
            if(! precharge.estPointVirgule()){
                return false;
            }
            precharge = lexical.suivant();            
        }
        
        return true;
    }
    
    /**
     * 
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estRegle() throws IOException {
        return estRegleSansPremisse() || estRegleAvecPremisse();
    }
    
    
    /**
     * 
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estRegleSansPremisse() throws IOException {
        if( estConclusionBooleenne() ){
            Regles regle = new RegleSansPremisse( conclusion );
            regle.ecrireSuccesseur(produit);
            produit = regle;
            return true;
        }
        
        if( estConclusionSymbolique() ){
            Regles regle = new RegleSansPremisse( conclusion );
            regle.ecrireSuccesseur(produit);
            produit = regle;
            return true;
        }
        
        if( estConclusionEntiere() ){            
            Regles regle = new RegleSansPremisse( conclusion );
            regle.ecrireSuccesseur(produit);
            produit = regle;
            return true;
        }
        
        return false;
    } 
    
    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estRegleAvecPremisse() throws IOException {
        
        if(! precharge.estOperateurSi() ){
            return false;
        }
        
        precharge = lexical.suivant();
        
        if( estCondition() ){
        
            if(! precharge.estOperateurAlors() ){
                return false;
            }

            precharge = lexical.suivant();

            if( estConclusion() ){
                if(! precharge.estPointVirgule()){
                    return false;
                }    
                precharge = lexical.suivant();                
                Regles regle = new RegleAvecPremisse( condition, conclusion );
                regle.ecrireSuccesseur(produit);
                produit = regle;
                return true;
            } 
            return false;
        }
        
        return false;
        
        
    }
    
    
    
    /* les règles régissant les conclusions dans la grammaire Lorraine */
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estConclusion() throws IOException {
        return estConclusionBooleenne() || estConclusionSymbolique() || estConclusionEntiere();
    }    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estConclusionBooleenne() throws IOException {
        if( estFaitBooleen() ){
            if(! faits_Booleens.contains(identificateur) ){
                //Correction semantique.
                errorMessage = "["+identificateur+"] n'est pas un fait booleen.";
                return false;
            }
            
            conclusion = new ConclusionAffirmative(identificateur);
            return true;
        }
        
        if( precharge.estOperateurNegation() ){
            
            precharge = lexical.suivant();
            
            if( estFaitBooleen() ){
                if(! faits_Booleens.contains(identificateur) ){
                    //Correction semantique.
                    errorMessage = "["+identificateur+"] n'est pas un fait booleen.";
                    return false;
                }
                conclusion =  new ConclusionNegative(identificateur);
                return true;                
            }
            //Correction semantique.
            errorMessage = "["+identificateur+"] n'est pas un fait booleen.";
            return false;
        }
        errorMessage = "["+identificateur+"] n'est pas un fait_booleen";
        return false;
    }
    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estConclusionSymbolique() throws IOException {
        
        if( estFaitSymbolique() ){
            //correction semantique.
            if(! faits_Symboliques.contains(identificateur) ){
                errorMessage = "["+precharge.lireRepresentation()+"] n'est pas un fait_symbolique";
                return false;
            }
            String nomFait = identificateur;
            if(! precharge.estComparateurEgalite()){
                return false;
            }
            
            precharge = lexical.suivant();
            
            //correction semantique
            if( estIdentificateur() ){
                if( (faits_Symboliques.contains(identificateur))&&(
                        faits_Entiers.contains(identificateur) || 
                        faits_Booleens.contains(identificateur) )){
                    errorMessage = "["+identificateur+"] ne doit pas etre un nom "
                            + "de fait_Entier ou fait_Booleen";
                    return false;
                }
                
                conclusion = new ConclusionConstanteSymbolique(nomFait, identificateur);
                return true;
            }
            errorMessage = "[=] est doit etre suivi d'un fait_symbolique";
            return false;
        }
        
        if( estFaitSymbolique() ){
            
            //correction semantique.
            if(! faits_Symboliques.contains(identificateur) ){
                errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
                return false;
            }
            String nomFait = identificateur;
            if(! precharge.estComparateurEgalite()){
                errorMessage = "Caractere attendu est : [=] au lieu de "+precharge.lireRepresentation();
                return false;
            }
            
            precharge = lexical.suivant();
            if( estFaitSymbolique() ){
                
                if( (faits_Entiers.contains(identificateur) || 
                        faits_Booleens.contains(identificateur))&&(
                        faits_Symboliques.contains(identificateur)) ){
                    
                    errorMessage = "["+identificateur+"] ne doit pas etre un nom "
                            + "de fait_Entier ou fait_Booleen";
                    return false;
                }
                
                conclusion = new ConclusionSymboliqueFait(nomFait, identificateur);
                return true;
            }
            errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
            return false;
        }
        errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
        return false;
    }
    

    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estConclusionEntiere() throws IOException {
        
        if( estFaitEntier() ){
            String nomFait = identificateur;
           //correction semantique.
            if(! faits_Entiers.contains(nomFait) ){
                errorMessage = "["+identificateur+"] doit etre un nom "
                        + "de fait_Entiers";
                return false;
            }
            if(! precharge.estComparateurEgalite()){
                errorMessage = "Caractere attendu est : [=] au lieu de "+precharge.lireRepresentation();
                return false;
            }

            precharge = lexical.suivant();

            
            if( estIdentificateur() ){
                //correction semantique.
                if( faits_Symboliques.contains(identificateur) || faits_Booleens.contains(identificateur) ){
                    errorMessage = "["+identificateur+"] ne doit pas etre un nom "
                            + "de fait_Symbolique ou fait_Booleen";
                    return false;
                }
                
                conclusion = new ConclusionFaitEntier(nomFait, identificateur);
                return true;                
            }
            
            if( estExpressionEntiere() ){
                conclusion = new ConclusionEntiere(nomFait, expr);
                return true;                
            }

            return false;
        }
        
        return false;
    }
    
    
    
    /**
     * Methode la regle Constante Symbolique
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estConstanteSymbolique() throws IOException {
        if( estFaitSymbolique() ){
            this.faits_Symboliques.add(identificateur);
            return true;
        }
        return false;
    }
    
    
   /**
     * Methode associee a la regle "ExpressionEntiere".
     *
     * @return true si c'est une expression false sino.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression.
     */
    protected boolean estExpressionEntiere() throws IOException {
	        
	// Test de l'operateur additif optionnel.
	if (precharge.estOperateurPlus() || precharge.estOperateurMoins()) {
            
            if(precharge.estOperateurPlus()){
                // L'operateur est present : il faut passer au jeton suivant.
                precharge = lexical.suivant();
                expr = new OperateurAdd( new FeuilleEntiere(0), expr );
            } else{
                // L'operateur est present : il faut passer au jeton suivant.
                precharge = lexical.suivant();                
                expr = new OperateurSub( new FeuilleEntiere(0), expr );
            }
	}
        
        if(! estTerme() ){
            return false;
        }
        //ancienne valeur de l'expression
        ExpressionEntiere expression = expr;
	// Sequences eventuelles composees d'un operateur additif suivi d'un
	// terme.
	while (precharge.estOperateurPlus() || precharge.estOperateurMoins()) {

            if(precharge.estOperateurPlus()){
                // L'operateur est present : il faut passer au jeton suivant.
                precharge = lexical.suivant();
                if(! estTerme() ){
                    return false;
                }   
                expr = new OperateurAdd( expression, expr );
            } else{
                // L'operateur est present : il faut passer au jeton suivant.
                precharge = lexical.suivant();
                if(! estTerme() ){
                    return false;
                }
                expr = new OperateurSub( expression, expr );
            }

	}

	// La regle expression est satisfaite.
	return true;
    }

    /**
     * Methode associee a la regle "Terme".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     * l'expression
     */
    protected boolean estTerme() throws IOException {
	
        // Appel de la methode associee a la regle "Facteur".
        if(! estFacteur() ){
            return false;
        }
        
        ExpressionEntiere expression = expr;

	// Sequences eventuelles composees d'un operateur multiplicatif suivi
	// d'un facteur.
	while ( precharge.estOperateurMultiplie() || precharge.estOperateurDivise() ) {

            if(precharge.estOperateurMultiplie()){
                // L'operateur est present : il faut passer au jeton suivant.
                precharge = lexical.suivant();
                // Appel de la methode associee a la regle "Facteur".
                if (! estFacteur()) {
                    return false;
                }                
                expr = new OperateurMult( expression, expr );
            } else{
                // L'operateur est present : il faut passer au jeton suivant.
                precharge = lexical.suivant();
                // Appel de la methode associee a la regle "Facteur".
                if (! estFacteur()) {
                    return false;
                }                
                expr = new OperateurDiv( expression, expr );
            }
            
	}

	// La regle est satisfaite.
	return true;	
    }

    
    
    /**
     * Methode associee a la regle "Facteur".
     *
     * @return true si la regle est satisfaite sinon false.
     * @throws IOException si l'analyseur lexical ne parvient pas a lire
     *   l'expression.
     */
    protected boolean estFacteur() throws IOException {

	// Le jeton precharge est un entier.
	if ( estConstanteEntiere() ) {
            
	    // La regle est satisfaite.
	    return true;//new FeuilleEntiere(Long.parseLong(precharge.lireRepresentation()));
	}

        // Le jeton precharge est un fait entier.
	if ( estFaitEntier() ) {
            expr = new FeuilleFaitEntier(identificateur);
	    // La regle est satisfaite.
	    return true;
	}
        
        // Si le jeton precharge est une parenthese ouvrante, il s'agit d'une
	// nouvelle expression parenthesee.
	if (! precharge.estParentheseOuvrante()) {
            errorMessage = "Caractere attendu est : [(] au lieu de "+precharge.lireRepresentation();
            return false;
        }
        // Passer la parenthese.
        precharge = lexical.suivant();

        // Appel de la methode associee a la regle "Expression_Entiere".
        //ExpressionEntiere expr = estExpressionEntiere();
        if(! estExpressionEntiere() ){
            return false;
        }

        // Le jeton precharge doit etre une parenthese fermante.
        if (! precharge.estParentheseFermante()) {
            errorMessage = "Caractere attendu est : [)] au lieu de "+precharge.lireRepresentation();
            return false;
        }

        // Passer la parenthese fermante.
        precharge = lexical.suivant();

        // La regle est satisfaite.
        return true;

    }
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estConstanteEntiere() throws IOException {
        
        if( precharge.estZero() ){
            precharge = lexical.suivant();
            expr = new FeuilleEntiere(0);
            return true;
        }
        
        if( precharge.estChiffre_Positif() ){
            identificateur = precharge.lireRepresentation();
            precharge = lexical.suivant();
            while( estChiffre() ){
                identificateur += precharge.lireRepresentation();
                precharge = lexical.suivant();
            }
            expr = new FeuilleEntiere(Long.parseLong(identificateur));
            return true;
        }
        
        return false;
    }
    
    
    /* Les règles régissant les conditions dans la grammaire Lorraine */

    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estCondition() throws IOException {
        condition = new LinkedList<Forme>();
        if( estPremisse() ){
            return false;
        }
        
        while( precharge.estOperateurEt() ){
        
            precharge = lexical.suivant();
            
            if(! estPremisse() ){
                return false;
            }
        }
        
        return true;
    }
    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estPremisse() throws IOException {
        return estPremisseBooleenne() || estPremisseSymbolique() || estPremisseEntiere();
    }    
    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estPremisseBooleenne() throws IOException {
        
        if( estFaitBooleen() ){
            if(! faits_Booleens.contains(identificateur) ){
                //Correction semantique.
                errorMessage = "["+identificateur+"] n'est pas un fait booleen.";
                return false;
            }

            condition.add(new PremisseAffirmation(identificateur));
            return true;
        }
        
        if( precharge.estOperateurNegation() ){
            
            precharge = lexical.suivant();
            
            if( estFaitBooleen() ){
                if(! faits_Booleens.contains(identificateur) ){
                    //Correction semantique.
                    errorMessage = "["+identificateur+"] n'est pas un fait booleen.";
                    return false;
                }
                condition.add(new PremisseNegation(identificateur));
                return true;                
            }

            return false;
        }
        errorMessage = "["+identificateur+"] n'est pas un fait_booleen";
        return false;
    }    
    

    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estPremisseSymbolique() throws IOException {
        
        if( estFaitSymbolique() ){
            //correction semantique.
            if(! faits_Symboliques.contains(identificateur) ){
                errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
                return false;
            }
            String nomFait = identificateur;
            if(! precharge.estComparateurEgalite()){
                return false;
            }
            
            precharge = lexical.suivant();
            if( estConstanteSymbolique() ){
                
                if( (faits_Entiers.contains(identificateur) || 
                        faits_Booleens.contains(identificateur))&&(
                        faits_Symboliques.contains(identificateur)) ){
                    errorMessage = "["+identificateur+"] ne doit pas etre un nom "
                            + "de fait_Entier ou fait_Booleen";
                    return false;
                }
                condition.add(new PremisseConstanteSymbolique(nomFait, ComparateurSymbolique.Egalite, identificateur));
                return true;
            }
            
            return false;
        }
        
        if( estFaitSymbolique() ){
            
            //correction semantique.
            if(! faits_Symboliques.contains(identificateur) ){
                errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
                return false;
            }
            String nomFait = identificateur;
            if(! precharge.estComparateurDifferent()){
                errorMessage = "Caractere attendu est : [=] au lieu de "+precharge.lireRepresentation();
                return false;
            }
            
            precharge = lexical.suivant();
            if( estFaitSymbolique() ){
                
                if( faits_Entiers.contains(identificateur) || 
                        faits_Booleens.contains(identificateur)&&
                        (faits_Symboliques.contains(identificateur)) ){
                    errorMessage = "["+identificateur+"] ne doit pas etre un nom "
                            + "de fait_Entier ou fait_Booleen";
                    return false;
                }
                
                Regles regle = new RegleSansPremisse( new ConclusionSymboliqueFait(nomFait, identificateur) );
                regle.ecrireSuccesseur(produit);
                condition.add(new PremisseConstanteSymbolique(nomFait, ComparateurSymbolique.Different, identificateur));
                return true;
            }
            errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
            return false;
        }
        errorMessage = "["+identificateur+"] n'est pas un fait_symbolique";
        return false;
    }     
    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estPremisseEntiere() throws IOException {
        
        if( estFaitEntier() ){
            String nomFait = identificateur;
           //correction semantique.
            if(! faits_Entiers.contains(nomFait) ){
                errorMessage = "["+identificateur+"] doit etre un nom "
                        + "de fait_Entiers";
                return false;
            }
            
            if( estComparateur() ){
                ComparateurEntier compare;
                switch( precharge.lireRepresentation() ){
                    case ">":
                        compare = ComparateurEntier.Superieur;
                        break;
                    case ">=":
                        compare = ComparateurEntier.SuperieurOuEgal;
                        break;
                    case "<":
                        compare = ComparateurEntier.Inferieur;
                        break;
                    case "<=":
                        compare = ComparateurEntier.InferieurOuEgal;
                        break;
                    default:
                        compare = ComparateurEntier.Different;                        
                }
                
                precharge = lexical.suivant();

                if( estIdentificateur() ){
                    //correction semantique.
                    if( faits_Symboliques.contains(identificateur) || faits_Booleens.contains(identificateur) ){
                        errorMessage = "["+identificateur+"] ne doit pas etre un nom "
                                + "de fait_Symbolique ou fait_Booleen";
                        return false;
                    }
                    
                    condition.add(new PremisseFaitEntier(nomFait, compare, identificateur));
                    return true;                
                }

                if( estExpressionEntiere() ){
                    condition.add(new PremisseConstanteEntiere(nomFait, compare, expr));
                    return true;                
                }
                
                return false;
            }
            return false;
        }
        return false;
    }    
    
    
    /**
     * 
     * @return
     * @throws IOException 
     */
    protected boolean estComparateur() throws IOException {
        return precharge.estComparateurInferieur() || precharge.estComparateurSuperieur() 
                || precharge.estComparateurInferieurOuEgal() || precharge.estComparateurSuperieurOuEgal() || 
                precharge.estComparateurEgalite() || precharge.estComparateurDifferent();
    }
    
     
    /**
     * 
     */
    protected LinkedList<String> faits_Booleens;
    
    /**
     * 
     */
    protected LinkedList<String> faits_Symboliques;

    /**
     * 
     */
    protected LinkedList<String> faits_Entiers;
    
    /**
     * 
     */
    protected LinkedList<Forme> condition ;
    
    
    /**
     * 
     */
    protected Forme conclusion;
    
    /**
     * 
     */
    protected String identificateur;

    /**
     * 
     */
    protected ExpressionEntiere expr;    
    
    /**
     * Le message d'erreur.
     */
    protected String errorMessage;
    
    /**
     * Analyseur lexical.
     */
    protected Lexical lexical;

    /**
     * Dernier jeton precharge.
     */
    protected Jeton precharge;  
    
    
    /**
     * Le produit a fabriquer.
     */
    protected Regles produit;

}
