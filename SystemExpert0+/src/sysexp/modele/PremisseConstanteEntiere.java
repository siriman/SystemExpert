
package sysexp.modele;

/**
 * Classe representant une premisse entiere avec une expression entiere comme 
 * partie droite.
 * 
 * @author siriman
 */
public class PremisseConstanteEntiere extends GeneriqueEntiereSymbolique < ExpressionEntiere > {

    /**
     * Le comparateur.
     */
    protected ComparateurEntier comparateur;
    
    /**
     * Constructeur logique
     * 
     * @param nom le nom du fait.
     * @param comparateur le comparateur.
     * @param partieDroite la partie droite de la premisse.
     */
    public PremisseConstanteEntiere(String nom, ComparateurEntier comparateur, ExpressionEntiere partieDroite) {
        super(nom, partieDroite);
        this.comparateur = comparateur;
    }
    
    @Override
    public boolean accept(VisiteurForme visiteur) throws DivisionParZeroException, IncoherenceException {
        return visiteur.visite(this);
    }
    
    /**
     * Accesseur
     * @return {@link #comparateur}
     */
    public ComparateurEntier lireComparateur() {
        return comparateur;
    }
    
}
