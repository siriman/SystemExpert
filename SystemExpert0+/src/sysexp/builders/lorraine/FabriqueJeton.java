
package sysexp.builders.lorraine;

/**
 * Classe representant la Fabrique des Jetons
 * 
 * @author siriman
 */
class FabriqueJeton {
    
    /**
     * Retourne le jeton chiffre positif.
     * 
     * @param representation une representation
     * @return le jeton chiffre positif
     */
    public static Jeton chiffre_Positif(String representation){
        return new Jeton(Jeton.Type.Chiffre_Positif, representation);
    }
    
    /**
     * Retourne le jeton associe a une lettre.
     *
     * @return le jeton associe a une lettre.
     */
    public static Jeton lettre(String representation){
        return new Jeton(Jeton.Type.Lettre, representation);
    }
    
    /**
     * Retourne le jeton associe au mot cle faits_Booleens .
     *
     * @return le jeton associe au faits_Booleens.
     */
    public static Jeton faits_Booleens(){
        return faits_booleens;
    }
    
    /**
     * Retourne le jeton associe au mot cle faits_Symboliques .
     *
     * @return le jeton associe au faits_Symboliques.
     */
    public static Jeton faits_Symboliques(){
        return faits_symboliques;
    }
    
    /**
     * Retourne le jeton associe au mot cle faits_entiers .
     *
     * @return le jeton associe au faits_entiers.
     */
    public static Jeton faits_Entiers(){
        return faits_entiers;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a loperateur ET.
     */
    public static Jeton operateurEt(){
        return operateurEt;
    }    
    
    
    /**
     * Retourne le jeton associe a inconnu.
     *
     * @return le jeton associe a inconnu.
     */
    public static Jeton inconnu(String representation){
        return new Jeton(Jeton.Type.Inconnu, representation);
    }
    
    /**
     * Retourne le jeton associe a zero.
     *
     * @return le jeton associe a zero.
     */
    public static Jeton zero(){
        return zero;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a l'operateur plus.
     */
    public static Jeton operateurPlus(){
        return operateurPlus;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurMoins(){
        return operateurMoins;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurMultiplie(){
        return operateurMultiplie;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurDivise(){
        return operateurDivise;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurAnd(){
        return operateurAnd;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurNegation(){
        return operateurNegation;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton comparateurSuperieur(){
        return comparateurSuperieur;
    }    
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton comparateurInferieur(){
        return comparateurInferieur;
    }    
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton comparateurEgalite(){
        return comparateurEgalite;
    }    

    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton comparateurSupEgal( String representation ){
        return new Jeton(Jeton.Type.ComparateurSuperieurOuEgal, representation);
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton comparateurDifferent( String representation ){
        return new Jeton( Jeton.Type.ComparateurDifferent, representation );
    }
    
    
    /**
     * Retourne le jeton associe au comparateur inferieur ou egale.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton comparateurInfEgal( String representation ){
        return new Jeton(Jeton.Type.ComparateurInferieurOuEgal, representation);
    }    

    /**
     * Retourne le jeton associe a la parenthese ouvrante.
     *
     * @return le jeton associe a la parenthese ouvrante.
     */
    public static Jeton parentheseOuvrante() {
	return parentheseOuvrante;
    }

    /**
     * Retourne le jeton associe a la parenthese fermante.
     *
     * @return le jeton associe a la parenthese fermante.
     */
    public static Jeton parentheseFermante() {
	return parentheseFermante;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton caracterePipe(){
        return caracterePipe;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton epsilon(){
        return epsilon;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton virgule(){
        return virgule;
    }    
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton pointVirgule(){
        return pointVirgule;
    }    
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton underscore(){
        return underscore;
    }    
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurSi(){
        return si;
    }    
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton operateurAlors(){
        return alors;
    }
    
    /**
     * Retourne le jeton associe a la fin d'expression.
     *
     * @return le jeton associe a la fin d'expression.
     */
    public static Jeton finExpression() {
	return finExpression;
    }
    
    /**
     * Jeton associe a zero.
     */
    protected static final Jeton zero = new Jeton(Jeton.Type.Zero, "0");
    
    /**
     * Jeton associe a plus .
     */
    protected static final Jeton operateurPlus = new Jeton(Jeton.Type.OperateurPlus, "+");
    
    /**
     * Jeton associe moin.
     */
    protected static final Jeton operateurMoins = new Jeton(Jeton.Type.OperateurMoins, "-");
    
    /**
     * Jeton associe a la multiplication .
     */
    protected static final Jeton operateurMultiplie = new Jeton(Jeton.Type.OperateurMultiplie, "*");
    
    /**
     * Jeton associe a la division .
     */
    protected static final Jeton operateurDivise = new Jeton(Jeton.Type.OperateurDivise, "/");
    
    /**
     * Jeton associe  .
     */
    protected static final Jeton operateurAnd = new Jeton(Jeton.Type.OperateurAnd, "and");
    
    /**
     * Jeton associe a "et" .
     */
    protected static final Jeton operateurEt = new Jeton(Jeton.Type.OperateurEt, "et");
    
    /**
     * Jeton associe a non .
     */
    protected static final Jeton operateurNegation = new Jeton(Jeton.Type.OperateurNegation, "non");
                       
    /**
     * Jeton associe a superieur .
     */
    protected static final Jeton comparateurSuperieur = new Jeton(Jeton.Type.ComparateurSuperieur, ">");
    
    /**
     * Jeton associe a inferieur .
     */
    protected static final Jeton comparateurInferieur = new Jeton(Jeton.Type.ComparateurInferieur, "<");
    
    /**
     * Jeton associe a l'affectation.
     */
    protected static final Jeton comparateurEgalite = new Jeton(Jeton.Type.ComparateurEgalite, "=");

    /**
     * Jeton associe a la parenthese ouvrante.
     */
    protected static final Jeton parentheseOuvrante = new Jeton(Jeton.Type.ParentheseOuvrante, "(");

    /**
     * Jeton associe a la parenthese fermante.
     */
    protected static final Jeton parentheseFermante = new Jeton(Jeton.Type.ParentheseFermante, ")");
    
    /**
     * Jeton associe au comparateur superieur ou egal.
     */
    protected static final Jeton comparateurSupEgal = new Jeton(Jeton.Type.ComparateurSuperieurOuEgal, ">=");
    
    /**
     * Jeton associe au comparateur inferieur ou egal .
     */
    protected static final Jeton comparateurInfEgal = new Jeton(Jeton.Type.ComparateurInferieurOuEgal, "<=");
    
    /**
     * Jeton associe a different.
     */
    protected static final Jeton comparateurDifferent = new Jeton(Jeton.Type.ComparateurDifferent, "/=");
    
    
    /**
     * Jeton associe au pipe.
     */
    protected static final Jeton caracterePipe = new Jeton(Jeton.Type.CaracterePipe, "|");
    
    /**
     * Jeton associe epsilon.
     */
    protected static final Jeton epsilon = new Jeton(Jeton.Type.Epsilon, "Ɛ");
    
    /**
     * Jeton associe a la virgule.
     */
    protected static final Jeton virgule = new Jeton(Jeton.Type.Virgule, ",");
    
    /**
     * Jeton associe au point virgule.
     */
    protected static final Jeton pointVirgule = new Jeton(Jeton.Type.PointVirgule, ";");
    
    /**
     * Jeton associe a underscore.
     */
    protected static final Jeton underscore = new Jeton(Jeton.Type.Underscore, "_");
    
    /**
     * Jeton associe au mot si.
     */
    protected static final Jeton si = new Jeton(Jeton.Type.Si, "si");
    
    /**
     * Jeton associe au mot alors .
     */
    protected static final Jeton alors = new Jeton(Jeton.Type.Alors, "alors");
    
    
    /**
     * Jeton associe au mot faits_booleens .
     */
    protected static final Jeton faits_booleens = new Jeton(Jeton.Type.MotCleFaits_Booleens, "faits_booleens");
    
    /**
     * Jeton associe a faits_symboliques.
     */
    protected static final Jeton faits_symboliques = new Jeton(Jeton.Type.MotCleFaits_Symboliques, "faits_symboliques");
    
    /**
     * Jeton associe a faits_entiers .
     */
    protected static final Jeton faits_entiers = new Jeton(Jeton.Type.MotCleFaits_Entiers, "faits_entiers");   
    
    /**
     * Jeton associe a la fin d'expression.
     */
    protected static final Jeton finExpression = 
	new Jeton(Jeton.Type.FinExpression, "");
    
}
