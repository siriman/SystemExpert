
package sysexp.modele;

/**
 * Classe representant l'operateur de multiplication.
 * 
 * @author siriman
 */
public class OperateurMult extends AbstractOperateur {

    /**
     * Constructeur logique.
     * 
     * @param operandeGauche expression de gauche.
     * @param operandeDroite expression de droite.
     */
    public OperateurMult( ExpressionEntiere operandeGauche, ExpressionEntiere operandeDroite ) {
        super(operandeGauche, operandeDroite);
    }

    /**
     * Methode faisant la multiplication de deux expressions.
     * 
     * @param contexte la base de fait.
     * @return le resultat de la multiplication de l'{@link #operandeGauche} par l'{@link #operandeDroite}.  
     * @throws DivisionParZeroException est lance quand l'{@link #operandeDroite} est inferieur a zero  
     * @throws IncoherenceException est lance quant on essayera d'evaluer un fait 
     *  inexistant dans la base de fait.
     */
    @Override
    public long evaluer(BaseDefaits contexte) throws DivisionParZeroException, IncoherenceException {
        return operandeGauche.interpret(contexte) * operandeDroite.interpret(contexte);
    }
    
}
