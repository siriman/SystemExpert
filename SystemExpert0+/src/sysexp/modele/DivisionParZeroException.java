
package sysexp.modele;

/**
 * Classe representant une erreur de division par zero.
 * 
 * @author siriman
 */
public class DivisionParZeroException extends Exception {

    /**
     * Constructeur par defaut.
     */
    public DivisionParZeroException() {
    }

    /**
     * Constructeur logique.
     *
     * @param msg le detail du message d'ereur.
     */
    public DivisionParZeroException(String msg) {
        super(msg);
    }
}
